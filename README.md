# reign-fullstack-test
This is a full-stack JavaScript web project, where the server fetch articles once an hour from a [Hacker News' API](https://hn.algolia.com/api/v1/search_by_date?query=nodejs), stores the response data in the database and exposes and API to the client; the client gets the data from the server and displays the articles in a list. This project contains:

- Client component (React)
- Server component (Node.js + NestJS + MongoDB + Mongoose)

### ️✔How to use?
1. Clone the repo
2. From the root of the project open a terminal
3. Make sure you have [docker-compose](https://docs.docker.com/compose/install/) installed on Linux. If you are not on Linux, you can install [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/) or [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/install/) and get docker-compose
4. Type: `docker-compose build`
5. Then: `docker-compose up`
6. When you see in the terminal that the client and the server are ready, go to your browser and type `localhost:3000` to open the client 

### ✔️ Running the project for the first time
Since the server fetches data once an hour, when you run the project for the first time the database will be empty, and you won't see any results on the client. However, if there are no articles in the database, you can press the **Refresh Data** button and force a data refresh

### ✔️ Considerations
- To prevent that articles were shown in the client page without a title or a url, the server filtered the data and saved in the database only the articles that have titles and urls
- The dates formats are based on local timezone
- A pipeline for *eslint* and *unit tests* was created for the server files, it runs for each commit in the repo
- For the frontend, only css and html were used