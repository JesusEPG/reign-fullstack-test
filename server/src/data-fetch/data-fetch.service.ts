import { Injectable, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Article } from '../articles/interfaces/article.interface';

@Injectable()
export class DataFetchService {
  
  constructor (
    private httpService: HttpService,
    @InjectModel('Article') private readonly articleModel: Model<Article>
  ) {}

  fetchDataFromApi(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise()
        .then(res => {
          const { data } = res;
          const filteredArticles = data.hits.filter(hit => ((hit.title || hit.story_title) && (hit.url||hit.story_url)));

          let articlesUpdate = filteredArticles.map(article => ({
            updateOne: {
              filter: {objectID: article.objectID},
              update: {$set: article},
              upsert: true
            }
          }));

          this.articleModel.bulkWrite(articlesUpdate)
          .then(bulkResponse => resolve(bulkResponse))
          .catch(bulkError => reject(new Error(bulkError)));
        })
        .catch(error => reject(new Error(error)));
    });
  }
}
