import { Module, HttpModule, } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ArticleSchema } from '../articles/schemas/article.schema';

import { DataFetchService } from './data-fetch.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }])
  ],
  providers: [DataFetchService],
  exports: [DataFetchService]
})
export class DataFetchModule {}
