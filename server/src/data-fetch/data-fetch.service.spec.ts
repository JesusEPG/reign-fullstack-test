import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';

import { DataFetchService } from './data-fetch.service';
import { closeInMongodConnection, rootMongooseTestModule } from '../../test/mongooseMockUtils';
import { ArticleSchema } from '../articles/schemas/article.schema';

class HttpServiceMock {
  get() {
    return true;
  }
 }

describe('DataFetchService', () => {
  let module: TestingModule;

  beforeEach(async () => {
    const HttpServiceProvider = {
      provide: HttpService,
      useClass: HttpServiceMock,
    };
    module = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
      ],
      providers: [HttpServiceProvider, DataFetchService],
    }).compile();
  });

  it('should be defined', () => {
    expect(module).toBeDefined();
  });

  afterEach(async () => {
    return await closeInMongodConnection();
  });
});
