import { Test, TestingModule } from '@nestjs/testing';
import { DataFetchService } from '../data-fetch/data-fetch.service';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { Article } from './interfaces/article.interface';

jest.mock('../articles/articles.service');
jest.mock('../data-fetch/data-fetch.service');

describe('ArticlesController', () => {
  let articlesService: ArticlesService;
  let module: TestingModule;
  let controller: ArticlesController;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [
        ArticlesService,
        DataFetchService
      ]
    }).compile();

    articlesService = module.get<ArticlesService>(ArticlesService);
    controller = module.get(ArticlesController);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe("Find Articles", () => { 
    it("should return an array of articles", async () => {
      const expectedResult: Article = {
        objectID: '',
        title: '',
        story_title: '',
        author: '',
        created_at: '',
        url: '',
        story_url: '',
        created_at_i: 1,
        inactive: false
      };
      
      const response = {
        message: 'OK',
        data: expectedResult
      }

      jest.spyOn(articlesService, "getArticles").mockResolvedValue(expectedResult);
      expect(await controller.getArticles()).toStrictEqual(response);
    }); 
    
    it("should throw Error if there was a problem with the query", async (done) => {
      jest.spyOn(articlesService, "getArticles").mockRejectedValue(new Error("error"));
      await controller.getArticles()
      .then(() => done.fail("Articles controller should return error of 404 but did not"))
      .catch((error) => {
        expect(error).toBeDefined();
        done();
      });
    });
  });

  describe("Delete Article", () => { 
    
    it("should throw Error if article to delete was not found", async (done) => {
      const articleToDelete = 0;
      jest.spyOn(articlesService, "getArticleAndUpdate").mockResolvedValue(null);
      await controller.deleteArticle(articleToDelete)
      .then(() => done.fail("Articles controller should return error of 404 but did not"))
      .catch((error) => {
        expect(error).toBeDefined();
        expect(error.status).toBe(404);
        done();
      });
    });

    it("should throw Error if article to delete was not found", async (done) => {
      const articleToDelete = 0;
      jest.spyOn(articlesService, "getArticleAndUpdate").mockRejectedValue(new Error("error"));
      await controller.deleteArticle(articleToDelete)
      .then(() => done.fail("Articles controller should return error of 404 but did not"))
      .catch((error) => {
        expect(error).toBeDefined();
        expect(error.status).toBe(503);
        done();
      });
    });
  });

});
