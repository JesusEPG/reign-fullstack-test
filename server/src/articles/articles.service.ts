import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Article } from './interfaces/article.interface';

@Injectable()
export class ArticlesService {
  
  constructor(@InjectModel('Article') private readonly articleModel: Model<Article>) {}

  getArticleAndUpdate(articleObjectId: string): Promise<Article> {
    return this.articleModel.findOneAndUpdate(
      {objectID: articleObjectId},
      {inactive: true},
      {new: true}
    ).exec();
  }
  
  getArticles(): Promise<Article> {  
    return this.articleModel
      .find({$or: [{ inactive: { $exists: false} }, {inactive: false}]})
      .sort({created_at_i: -1})
      .exec();
  }
}
