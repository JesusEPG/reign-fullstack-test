import { Document } from 'mongoose';

export interface Article extends Document {
  readonly objectID: string;
  readonly title: string;
  readonly story_title: string;
  readonly author: string;
  readonly created_at: string;
  readonly url: string;
  readonly story_url: string;
  readonly created_at_i: number;
  readonly inactive: boolean;
}