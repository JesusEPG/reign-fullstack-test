export class CreateArticleDTO {
  readonly title: string;
  readonly story_title: string;
  readonly author: string;
  readonly created_at: string;
}