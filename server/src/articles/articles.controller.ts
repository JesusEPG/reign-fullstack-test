import { 
  Controller ,
  Get,
  Res, 
  HttpStatus,
  HttpException,
  Delete,
  Param
} from '@nestjs/common';

import { ArticlesService } from './articles.service';
import { DataFetchService } from '../data-fetch/data-fetch.service';

@Controller('articles')
export class ArticlesController {
  
  constructor(
    private articleService: ArticlesService,
    private dataFetchService: DataFetchService  
  ) {}

  @Get()
  async getArticles() {
    try {
      const articles = await this.articleService.getArticles();
      return {
        message: 'OK',
        data: articles
      }
    } catch {
      throw new HttpException('Service Unavailable', HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

  @Get('refresh')
  async refreshDbData(@Res() res) {
    try {
      const fetchResponse = await this.dataFetchService.fetchDataFromApi();
      if(fetchResponse) {
        const articles = await this.articleService.getArticles();
        return res.status(HttpStatus.OK).json({
          error: null,
          message: "OK",
          data: articles
        });
      } else {
        throw new HttpException('There was a problem fetching the data. Service unavailable', HttpStatus.SERVICE_UNAVAILABLE);  
      }
    } catch(error) {
      throw new HttpException(
        error.response || 'There was a problem fetching the data. Service Unavailable', 
        error.status || HttpStatus.SERVICE_UNAVAILABLE
      );
    }
    
  }

  @Delete(':id')
  async deleteArticle(@Param('id') articleObjectId) {

    try {
      const updatedArticle = await this.articleService.getArticleAndUpdate(articleObjectId);

      if(updatedArticle) {
        const filteredArticles = await this.articleService.getArticles();
        
        const responseObj = {
          error: null,
          message: 'Article has been deleted successfully!',
          data: filteredArticles,
          updatedArticle
        }
        return responseObj;
        
      } else {
        throw new HttpException('There was a problem deleting the article. Article was not found', HttpStatus.NOT_FOUND);  
      }

    } catch (error) {
      throw new HttpException(
        error.response || 'There was a problem deleting the article. Service Unavailable', 
        error.status || HttpStatus.SERVICE_UNAVAILABLE
      );
    }
  }
}
