import { Schema,  } from 'mongoose';

export const ArticleSchema = Schema({
  objectID: String,
  title: String,
  story_title: String,
  author: String,
  created_at: String,
  url: String,
  story_url: String,
  created_at_i: Number,
  inactive: Boolean
});