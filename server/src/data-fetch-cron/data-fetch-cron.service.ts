import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';

import { DataFetchService } from '../data-fetch/data-fetch.service';

@Injectable()
export class DataFetchCronService {
  
  constructor(
    private dataFetchService: DataFetchService
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  runEveryHour() {
    this.dataFetchService.fetchDataFromApi();
  }
}
