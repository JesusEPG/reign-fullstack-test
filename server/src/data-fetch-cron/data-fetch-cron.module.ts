import { Module } from '@nestjs/common';

import { DataFetchModule } from '../data-fetch/data-fetch.module';
import { DataFetchCronService } from './data-fetch-cron.service';

@Module({
  imports: [DataFetchModule],
  providers: [DataFetchCronService]
})
export class DataFetchCronModule {}
