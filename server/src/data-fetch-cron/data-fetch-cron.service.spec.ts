import { Test, TestingModule } from '@nestjs/testing';

import { DataFetchCronService } from './data-fetch-cron.service';
import { DataFetchService } from '../data-fetch/data-fetch.service';

class DataFetchServiceMock {
  fetchDataFromApi() {
    return true;
  }
}

describe('DataFetchCronService', () => {
  let module: TestingModule;
  let dataFetchCronService: DataFetchCronService;

  beforeAll(async () => {
    const DataFetchServiceProvider = {
      provide: DataFetchService,
      useClass: DataFetchServiceMock,
    };
    module = await Test.createTestingModule({
      providers: [DataFetchCronService, DataFetchServiceProvider],
    }).compile();
    dataFetchCronService = module.get<DataFetchCronService>(DataFetchCronService);
  });

  it('should be defined', () => {
    expect(dataFetchCronService).toBeDefined();
  });
});
