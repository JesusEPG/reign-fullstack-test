import { Module, HttpModule } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';

import { ArticlesModule } from './articles/articles.module';
import { DataFetchCronModule } from './data-fetch-cron/data-fetch-cron.module';
import { DataFetchModule } from './data-fetch/data-fetch.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://mongo/server-db', { useNewUrlParser: true }),
    ArticlesModule,
    HttpModule,
    DataFetchModule,
    DataFetchCronModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
