import Header from './components/Header/Header';
import ArticlesList from './components/ArticlesList/ArticlesList';

function App() {
  return (
    <div className="App">
      <Header />
      <ArticlesList />
    </div>
  );
}

export default App;
