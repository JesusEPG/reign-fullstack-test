import axios from 'axios';

import { apiUrl } from '../config/config';

export const getArticles = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${apiUrl}/articles`)
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        const errorToReturn = error.response ? error.response.data : new Error('There was a problem deleting the article. Please, try again later');
        reject(errorToReturn);
      });
  });
}

export const refreshArticles = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${apiUrl}/articles/refresh`)
      .then(response => resolve(response.data))
      .catch(error => {
        const errorToReturn = error.response ? error.response.data : new Error('There was a problem deleting the article. Please, try again later');
        reject(errorToReturn);
      });
  });
}

export const deleteArticle = (articleObjectId) => {
  return new Promise((resolve, reject) => {
    axios.delete(`${apiUrl}/articles/${articleObjectId}`)
      .then(response => resolve(response.data))
      .catch(error => {
        const errorToReturn = error.response ? error.response.data : new Error('There was a problem deleting the article. Please, try again later');
        reject(errorToReturn);
      });
  });
}