import React, { useState } from 'react';

import { dateFormatter } from '../../utils/dateFormatter';

import trash from './trashCan.png';

const ListItem = ({article, articles, index, removeArticle}) => {
  const [hovered, setHovered] = useState(false);

  const handleClick = (event) => {
    event.stopPropagation();
    removeArticle(article.objectID);
  }

  const handleRowClick = () => {
    window.open(article.story_url||article.url, '_blank')
  }

  return (
    <div 
      onClick={handleRowClick} 
      className={`full-article-row ${index+1 === articles.length ? '' : "row-border-bottom"}`}
      onMouseOver={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}    
    >
      <div className="article-row-container">
        <div className="first-column">
          <span className="title">{article.story_title||article.title}</span>
          <span className="author">- {article.author} -</span>
        </div>
        <div className="second-column">
          <span className="date">{dateFormatter(article.created_at)}</span>
        </div>
        <div onClick={handleClick} className="third-column">
          <img className={`${hovered? 'icon' : 'icon-hidden'}`} src={trash} width={20} alt="trash-can" />
        </div>
      </div>
    </div>
  )
}

export default ListItem