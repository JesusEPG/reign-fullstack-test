import React, { Fragment } from 'react';

import ListItem from './ListItem';

const List = ({articles, removeArticle, refreshData}) => {
  
  const handleDataRefresh = () => refreshData();

  if (!articles.length) {
    return (
      <div className="empty-list-container">
        <h2 className="empty-list-text" >There are no articles at the moment 😔️</h2>
        <button onClick={handleDataRefresh} className="empty-list-button" type="button"> Refresh Data</button>
      </div>
    );
  }

  const articleRows = articles.map((article, index) => (
    <ListItem 
      key={article.objectID} 
      article={article} 
      articles={articles} 
      index={index} 
      removeArticle={removeArticle}
    />
  ));

  return (
    <Fragment>
      {articleRows}
    </Fragment>
  )
}

export default List;