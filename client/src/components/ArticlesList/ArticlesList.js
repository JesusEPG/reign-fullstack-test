import React, { useEffect, useState } from 'react';

import List from './List';
import Loader from '../Loader/Loader';
import AlertModal from '../AlertModal/AlertModal';

import { getArticles, deleteArticle, refreshArticles } from '../../services/articlesService';

import './style.css';

const ArticlesList = () => {
  const [fetching, setFetching] = useState(true);
  const [articles, setArticles] = useState([]);
  const [visible, setVisible] = useState(false);
  const [alertMessage, setAlerMessage] = useState('Error muy largo que se mostrara en el modal y que espero que se pase a una segunda lineas');

  useEffect(() => {
    getArticles()
      .then((response) => {
        setArticles(response.data);
        setFetching(false);
      })
      .catch(() => setFetching(false));
  }, []);

  const removeArticle = (articleObjectId) => {
    setFetching(true);
    deleteArticle(articleObjectId)
      .then(updatedArticles => {
        setArticles(updatedArticles.data);
        setFetching(false);
      })
      .catch(error => {
        setAlerMessage(error.message);
        setVisible(true);
        setFetching(false);
      });
  }

  const refreshData = () => {
    setFetching(true);
    refreshArticles()
      .then(newArticles => {
        setArticles(newArticles.data);
        setFetching(false);
      })
      .catch(error => {
        setAlerMessage(error.message);
        setVisible(true);
        setFetching(false);
      });
  }

  if(fetching) {
    return (
      <Loader />
    )
  }

  return (
    <div className="container">
      <div className="centered-column">
        <List articles={articles} removeArticle={removeArticle} refreshData={refreshData} />
      </div>
      <AlertModal visible={visible} setVisible={setVisible} message={alertMessage}/>
    </div>
  )
}

export default ArticlesList;