import React from 'react';

import './style.css';

const AlertModal = ({visible, setVisible, message}) => {

  const closeModal = () => setVisible(false);

  if(!visible){
    return null;
  }

  return (
    <div id="myModal" className="modal">
      <div className="modal-content">
        <p className="modal-message">{message}</p>
        <div className="close-container">
          <span onClick={closeModal} className="close">&times;</span>
        </div>
      </div>
    </div>
  )
}

export default AlertModal;