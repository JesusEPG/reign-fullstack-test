import React from 'react';

import './style.css';

const Header = () => {
  return (
    <header className="app-header">
      <div className="header-text-container">
        <h1 className="header-main-title">
          HN Feed
        </h1>
        <p className="header-sub-title">
          We ❤️ Hacker News
        </p>
      </div>
    </header>
  );
}

export default Header;