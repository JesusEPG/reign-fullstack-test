import { DateTime } from 'luxon';

export const dateFormatter = (originalString) => {
  const todayStart = DateTime.local().setLocale('en').startOf('day');
  const todayEnd = DateTime.local().setLocale('en').endOf('day');
  
  const yesterdayStart = DateTime.local().minus({day: 1}).setLocale('en').startOf('day');
  const articleDate = DateTime.fromISO(originalString).setLocale('en');
  
  if (articleDate >= todayStart && articleDate <= todayEnd) {
    return articleDate.toLocaleString({hour: '2-digit', minute: '2-digit'});
  } else if (articleDate >= yesterdayStart && articleDate < todayStart){
    return 'Yesterday';
  } else {
    return articleDate.toLocaleString({month: 'short', day: '2-digit'});
  }
}