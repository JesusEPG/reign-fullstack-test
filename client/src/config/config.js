const apiHost = process.env.API_HOST || 'http://localhost';
const apiPort = process.env.API_PORT || '5000';

export const apiUrl = `${apiHost}:${apiPort}`;
